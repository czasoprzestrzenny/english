<meta charset="utf-8" />
<?php include('lib/layoutOpen.php');?>
<script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>
<div class="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="pl_PL" data-type="horizontal" data-theme="light" data-vanity="jarosław-stary-6787b99b"><a class="LI-simple-link" href='https://pl.linkedin.com/in/jaros%C5%82aw-stary-6787b99b?trk=profile-badge'>Jarosław Stary</a></div>
<?php
session_start();
require_once 'lib/Model.class.php';
require_once 'lib/WordModel.class.php';
require_once 'config.php';
$user= $_SESSION['user_id'];
$wm = new WordModel ();
include('lib/layoutOpen.php');
include('lib/logoff.php');
include('lib/return_index.php'); ?>

<p class="text-primary">
1.Dodaj, którykolwiek artykuł do nauki.<br>
<br>2.Aplikacja losowo będzie wyświetlać słówka do nauki. Wybierz przycisk <mark>Rozwiń i sprawdź czy wiesz</mark>, aby zweryfikować czy dobrze rozumiesz wyświetlone słowo.<br>
<br>3.Następnie wybierz przycisk  <mark>I Known word</mark> lub <mark>I don't known word </mark>, a aplikacja zapisze i zapamięta czy znałeś słówko czy nie. <br>
W ten sposób buduje się historia Twojej nauki, którą możesz  monitorować używając opcji <mark>ranking</mark>, gdzie  możesz sprawdzić, które słówka wymagają większej uwagi,<br>
a które nie. Będą wyświetlane u góry strony te słowa, które wymagają największej uwagi, a na dole strony te, które już znasz i nie wymagają kolejnej powtórki.
<br>Opcja ranking działa dwojako: zarówno wyświetla jeden wybrany artykuł<mark> Ranking słówek wg artykułu </mark> lub całą historię aktywnych artykułów <mark> Ranking wszystkich przypisanych słówek </mark>
<br><br>4.Pozostałe funkcjonalności w menu  wyświetlają komentarze i nie wymagają dodatkowej infromacji.<br>
<br> Strona będzie sukcesywnie rozbudowywana o nowe statystyki i zestawienia wspomagające naukę. Jeżeli posiadasz jakieś pomysły, które mogłyby usprawnić działanie strony, <br>
proszę o kontakt za pośrednictwem formularza kontaktowego.<br><br>
Jarek.
</p>
<?php include('lib/layoutClose.php');?>
