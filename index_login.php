<meta charset="utf-8" />
<link rel="Stylesheet" type="text/css" href="style/login.css" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
</head>

<?php include('lib/layoutOpen.php');?>
<?php include('jquery_select.php');?>
<?PHP //Formularz logowania 

session_start();
function form(){
	echo '
		
		<div class = "container">
	<div class="wrapper">
		<form action="" method="post" name="Login_Form" class="form-signin">       
		    <h3 class="form-signin-heading">Witaj w aplikacji do nauki słówek</h3>
			  <hr class="colorgraph"><br>
			  
			  <input type="text" class="form-control" name="login" placeholder="login użytkownika" required="" autofocus="" />
			  <input type="password" class="form-control" name="password" placeholder="Hasło" required=""/>     		  
			  <button class="btn btn-lg btn-primary btn-block"  name="wyslano" value="login" type="submit">Login</button>  			
			  			
		<div class="login-help">
		<br>
		<a href="registry.php">Załóż konto</a> - <a href="Restore_Password.php">Przypomnij hasło</a> - <a href="form.php">Kontakt</a>
		</div>
	
	</div>
</div>
		
		
    ';
}
//Połączenie z bazą danych MySQL PDO
$db = new PDO('mysql:host=localhost;dbname=jarcyk_learn_english', 'jarcyk_jarcyk', 'admin1', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")) or die();
 
if(isset($_POST['wyslano'])){ //Sprawdzamy, czy submit został wciśnięty
	//Usuwamy białe znaki z przesłanych danych
	$login = trim($_POST['login']);
	$password = trim($_POST['password']);




	//Sprawdzamy czy użytkownik o podanych danych istnieje
	$stmt = $db->prepare("SELECT * FROM users WHERE login=:login AND password=:password");
	$stmt->bindValue(":login", $login, PDO::PARAM_STR);
	$stmt->bindValue(":password", $password, PDO::PARAM_STR);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	if($stmt->rowCount()!=0){
		echo '<div class="alert alert-success" role="alert"> Zalogowałeś się </div>';
		/*
		 * Tworzymy sesję dla zalogowanego użytkownika z:
		 * - informacją, że użytkownik jest zalogowany
		 * - jego id
		 */
		session_start();
		$_SESSION['logged'] = true;
		$_SESSION['user_id'] = $row['login'];
		session_write_close(); ?>
		<meta http-equiv="refresh" content="1; quiz_ask.php">
		<?php 
			}
	else{
		echo '<div class="alert alert-danger"  role="alert" > Login lub hasło nieprawidłowe</div>';
		form();
	}
}
else form();




?>

<?php $_SESSION['tablica'] = array();



