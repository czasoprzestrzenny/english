
<!DOCTYPE html>
<html>
<head>
<link rel="Stylesheet" type="text/css" href="style/login.css" />
<meta charset="utf-8">
<title>Formularz kontaktowy</title>
</head>
<body>
<?php include('lib/layoutOpen.php');

require_once 'lib/contactform.class.php';
require_once 'lib/Model.class.php';
require_once 'lib/WordModel.class.php';
require_once 'config.php';
$wm = new WordModel ();

$errors = false;
$success = false;

if(isset($_POST['sent'])){
$pass_msg = $wm->Restore_Password($_POST['email'], $_POST['user']);
$contactForm = new ContactForm;
$success = $contactForm->execute_password($pass_msg);
$errors = $contactForm->getErrors();
}
?>
	<div class = "container">
	<div class="wrapper">
		<form action="" method="post" name="Login_Form" class="form-signin">       
		    <h3 class="form-signin-heading">Formularz przypomnienia hasła</h3>
					  <hr class="colorgraph"><br>
		
		      <input type="text" class="form-control" name="user" placeholder="Login użytkownika" required="" autofocus="" />
				  <input type="email" name="email" id="email" class="form-control" placeholder="Adres email"/>
			  <button class="btn btn-lg btn-primary btn-block"  name="sent" type="submit">Przypomnij hasło</button> 
			  
			  <div class="login-help">
		<br>
		<a href="form.php">Jeżeli nie jesteś w stanie przywrócić hasła skorzystaj z formularza kontaktowego</a> 
		</div>
		</form>
	</div> 			
 		
	</div>
</div>				


				
				