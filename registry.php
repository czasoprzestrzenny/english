<html>
<head>
<link rel="Stylesheet" type="text/css" href="style/login.css" />
<meta charset="utf-8" />
</head>
<body>
<?php include('lib/layoutOpen.php');?>
<?PHP
//Formularz rejestracji
function form(){
	echo '		
		
		<div class = "container">
	<div class="wrapper">
		<form action="" method="post" name="Login_Form" class="form-signin">       
		    <h3 class="form-signin-heading">Załóż konto</h3>
					  <hr class="colorgraph"><br>
		
		      <input type="text" class="form-control" name="login" placeholder="Login użytkownika" required="" autofocus="" />
			  <input type="password" class="form-control" name="password" placeholder="Wprowadź hasło" required=""/>   
		  	  <input type="password" class="form-control" name="password2" placeholder="Powtórz hasło" required=""/> 
			  <input type="email" name="email" id="email" class="form-control" placeholder="Adres email"/>
			  <button class="btn btn-lg btn-primary btn-block"  name="wyslano" value="login" type="submit">Załóż konto</button>  			
 		
	</div>
</div>
		 ';
}

//Połączenie z bazą danych MySQL PDO
$db = new PDO('mysql:host=localhost;dbname=jarcyk_learn_english', 'jarcyk_jarcyk', 'admin1', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")) or die();

if(isset($_POST['wyslano'])){ //Sprawdzamy, czy submit został wciśnięty
	//Usuwamy białe znaki z przesłanych danych
	$login = trim($_POST['login']);
	$password = trim($_POST['password']);
	$password2 = trim($_POST['password2']);
	$email = trim($_POST['email']);
	 
	$errors = NULL; //Tworzymy zmienną przechowująca ewentualne błędy
	 
	//Sprawdzamy, czy przesłane dane mają odpowiednią ilość znaków
	if(strlen($login)<3) $errors .= 'Login musi zawierać co najmniej 3 znaki<br>';
	if(strlen($password)<4) $errors .= 'Hasło musi zawierać co najmniej 6 znaków<br>';
	if($password!==$password2) $errors .= 'Hasła nie są takie same<br>';
	if(!preg_match('/\@/', $email) || strlen($email)<5) $errors .= 'Podany adres e-mail jest nieprawidłowy<br>';
	 
	//Sprawdzamy czy użytkownik o takim samym loginie już nie istnieje
	$stmt = $db->prepare("SELECT COUNT(id) FROM users WHERE login=:login");
	$stmt->bindValue(":login", $login, PDO::PARAM_STR);
	$stmt->execute();
	$row = $stmt->fetch();
	if($row[0]>0) $errors .= 'Konto o takim loginie już istnieje<br>';
	 
	//Sprawdzamy czy użytkownik o takim samym adresie e-mail już nie istnieje
	$stmt2 = $db->prepare("SELECT COUNT(id) FROM users WHERE email=:email");
	$stmt2->bindValue(":email", $email, PDO::PARAM_STR);
	$stmt2->execute();
	$row2 = $stmt2->fetch();
	if($row2[0]>0) $errors .= 'Konto o takim adresie e-mail już istnieje<br>';
	 
	if(empty($errors)){ //Jeśli nie ma błędów, rejestrujemy użytkownika
		
		$dodaj = $db->prepare("INSERT INTO users VALUES(null, :login, :password, :email)");
		$dodaj->bindValue(":login", $login, PDO::PARAM_STR);
		$dodaj->bindValue(":password", $password, PDO::PARAM_STR);
		$dodaj->bindValue(":email", $email, PDO::PARAM_STR);
		$dodaj->execute();
		echo '<div class="alert alert-success" role="alert"> Zarejestrowałeś się. Możesz się teraz zalogować </div>';
		?>
		<meta http-equiv="refresh" content="1; index_login.php">
<?php 	}
	else{

		echo '<div class="alert alert-danger"  role="alert" > '.$errors.'</div>';
		form(); //Wyświetlamy formularz
	}
}
else form();
?>

</body>