<?php

class WordModel extends Model {
	protected $sql = array('countWords_user' => 'SELECT count(word) AS countWords_user FROM words where id_art in 
												(select id_art from user_art where user= :user)',
							'countWords' => 'SELECT count(word) AS countWords FROM words ',
							'randomWords' => 'SELECT word, translate, id_word FROM words where id_art in 
											 (select id_art from user_art where user= :user) ORDER BY RAND() LIMIT 1',
							'word' => 'SELECT * FROM words where id_word = :id_word',
							'i_valid' => 'INSERT INTO validate (id_word, id_art, word, user, date, test, test_true, test_false) 
									   	  VALUES (:id_word, :id_art, :word, :user, :date, :test, :test_true, :test_false)',
							'max_unknown' => 'select words.word, words.translate, sum(test) as theworst FROM words, validate  
											  where words.id_word=validate.id_word and user= :user and validate.id_art in 
											  (select id_art from user_art where user= :user) group by word, translate 
											   having theworst <0 order by theworst limit 2',
							'unchooseArt' => 'SELECT count(distinct id_art) as ile FROM words where id_art not in (select distinct
											  id_art from user_art where user= :user)',
							'Art' => 'SELECT distinct id_art, title as ile FROM words where id_art not in (select distinct
											  id_art from user_art where user= :user) ORDER BY id_art',
							'i_article' => 'INSERT INTO user_art (user, id_art, date)
									   	  VALUES (:user, :id_art, :date)',
							'Repeat_Art' => 'SELECT distinct  title, id_art FROM words where id_art in (select distinct
											  id_art from user_art where user= :user)',
							'title_article' => 'SELECT title FROM words where id_art= :id_art',
							'Repeat_Show' => 	 'SELECT words.word, words.translate, sum(test) as test_, sum(test_false) as false_, sum(test_true) as true_  
											FROM words left join validate on( words.id_word=validate.id_word) where validate.user= :user and words.id_word in (select distinct id_word from validate where user= :user and id_art= :id_art)
											 group by words.word, words.translate order by test_',
							'Repeat_Show_All' => 'SELECT words.word, words.translate, sum(test) as test_, sum(test_false) as false_, sum(test_true) as true_  
											FROM words left join  validate on (words.id_word=validate.id_word) where validate.user= :user and words.id_word in (select distinct id_word from validate where user= :user and id_art in(select id_art from user_art where user= :user))
											group by words.word, words.translate order by test_',
							'D_Art'=> 'DELETE FROM user_art WHERE id_art= :id_art and user= :user',
							'Art_not' => 'SELECT distinct id_art, title as ile FROM words where id_art  in (select distinct
											  id_art from user_art where user= :user) ORDER BY ile',
							'D_Val_Words'=> 'DELETE FROM validate WHERE id_art= :id_art and user= :user',
							'Show_Words' => 'SELECT word, translate FROM words where id_art= :id_art',
							'Site_Show' => 'SELECT website from words where id_art= :id_art',
							'Restore_Password' => 'select password from users where login= :user and email= :email',
							'Show_art_and_article' => 'SELECT distinct title, id_art FROM words ',
							'CheckWord' => 'Select word, sum(test_true) as good, sum(test_false) as bad from validate
											where user= :user and id_word= :id_word group by word',
							'i_test' => 'INSERT INTO results (data, user, good, bad, licznik, percent)
									   	  	VALUES (:date, :user, :good, :bad, :licznik, :percent)',
							'show_results' => 'SELECT data, user, licznik, good, bad, percent FROM results order by percent desc, data'

	);
	
	
	
	function __construct() {
		parent::__construct();
/*		echo 'wywołanie konstruktora <br />';
		echo 'dsn = ' . $this::$dsn . ' <br />'; */
	}	
	
	function countWords() {
		$stmt = $this::$pdo->query($this->sql['countWords']);
		$row = $stmt->fetch();
		return 'Liczba wszystkich słówek których możesz się uczyć:  ' . $row['countWords'];
	}
	
	function countWords_user($user) {
		$stmt = $this::$pdo->prepare($this->sql['countWords_user']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch();
		return $row['countWords_user'];
	}
	
	function randomWords($user) {
		$stmt = $this::$pdo->prepare($this->sql['randomWords']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch();
		return $row;
					
	}	
	function randomWordsNoDup($user) {
	
		if((count($_SESSION['tablica']))>=$this->countWords_user($user)) {
			unset($_SESSION['tablica']);
			$_SESSION['tablica'] = array();
			echo 'echo_usunąłem_dane_mogą_wystąpić_słówka_ponownie';
		}	
	do {
	$result=$this->randomWords($user);

	} while (in_array($result, $_SESSION['tablica']));
	array_push($_SESSION['tablica'],$result);
	return $result;

	}
	
	function word($id_word) {
		$stmt = $this::$pdo->prepare($this->sql['word']);
		$stmt->bindValue('id_word',$id_word, PDO::PARAM_INT);
		$stmt->execute();
		$row = $stmt->fetch();
		return $row;
	}
	function i_valid($id_word, $id_art, $word, $user, $test, $test_true, $test_false) {
		$stmt = $this::$pdo->prepare($this->sql['i_valid']);
		$stmt->bindValue(':id_word', $id_word, PDO::PARAM_INT);
		$stmt->bindValue(':id_art', $id_art, PDO::PARAM_INT);
		$stmt->bindValue(':word', $word, PDO::PARAM_STR);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->bindValue(':date', date('Y-m-d'), PDO::PARAM_STR);
		$stmt->bindValue(':test', $test, PDO::PARAM_INT);
		$stmt->bindValue(':test_true', $test_true*1, PDO::PARAM_INT);
		$stmt->bindValue(':test_false', $test_false*1, PDO::PARAM_INT);
		$stmt->execute();
		return $row;
	}
			
		function max_unknown($user) {	
		$stmt = $this::$pdo->prepare($this->sql['max_unknown']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch();
		return ' Najgorsze słówko:  ' . $row['word'] . '  nie wiedziałeś : ' .$row['theworst']*-1 . ' razy. ';
					
	}
	
		function unchooseArt($user) {
		$stmt = $this::$pdo->prepare($this->sql['unchooseArt']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch();
		return 'Masz możliwość dodania do nauki :  ' . $row['ile'] . '  artykułów ';
			
	}
	

		function Art($user) {
		$stmt = $this::$pdo->prepare($this->sql['Art']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetchAll();
		return $row;
	}
		
		function i_article($user, $id_art) {
		$stmt = $this::$pdo->prepare($this->sql['i_article']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		foreach ($id_art as $row):
		$stmt->bindValue(':id_art', $row, PDO::PARAM_INT);
		$stmt->bindValue(':date', date('Y-m-d'), PDO::PARAM_STR);
		$stmt->execute();
		endforeach ;
		return $row;
					
	}
	
		function Repeat_Art($user) {
		$stmt = $this::$pdo->prepare($this->sql['Repeat_Art']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetchAll();
		return $row;
	}
	
		function Repeat_Show($id_art, $user) {
		$stmt = $this::$pdo->prepare($this->sql['Repeat_Show']);
		$stmt->bindValue(':id_art', $id_art*1, PDO::PARAM_INT);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetchAll();
		return $row ;
	}
		function Repeat_Show_All($user) {
		$stmt = $this::$pdo->prepare($this->sql['Repeat_Show_All']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetchAll();
		return $row ;
	}
	
		function title_article($id_art) {
		$stmt = $this::$pdo->prepare($this->sql['title_article']);
		$stmt->bindValue(':id_art', $id_art*1, PDO::PARAM_INT);
		$stmt->execute();
		$row = $stmt->fetch();
		return $row['title'];
	}
	
		function D_Art($user, $id_art) {
		echo ' Usunięto artykuł/y';
		$stmt = $this::$pdo->prepare($this->sql['D_Art']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		foreach ($id_art as $row):
		$stmt->bindValue(':id_art', $row, PDO::PARAM_INT);
		$stmt->execute();
		endforeach ;
		$row = $stmt->fetchAll();
		return $row ;

	}
	
	function D_Val_Words($user, $id_art) {
		echo 'Usunięto zapamiętaną historię nauki dla artykułu/ów - ';
		$stmt = $this::$pdo->prepare($this->sql['D_Val_Words']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		foreach ($id_art as $row):
		$stmt->bindValue(':id_art', $row, PDO::PARAM_INT);
		$stmt->execute();
		$this->D_Art($user, $id_art);
		endforeach ;
		$row = $stmt->fetchAll();
		return $row ;
	
	}
	
	function Art_not($user) {
		$stmt = $this::$pdo->prepare($this->sql['Art_not']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetchAll();
		return $row;
	}
	

		function Show_Words($id_art) {
		$stmt = $this::$pdo->prepare($this->sql['Show_Words']);
		$stmt->bindValue(':id_art', $id_art, PDO::PARAM_INT);
		$stmt->execute();
		$row = $stmt->fetchAll();
		return $row ;
	}
	
		function Site_Show($id_art) {
		$stmt = $this::$pdo->prepare($this->sql['Site_Show']);
		$stmt->bindValue(':id_art', $id_art, PDO::PARAM_INT);
		$stmt->execute();
		$row = $stmt->fetch();
		return $row['website'];

	}
	
	function Restore_Password($email, $user) {
		$stmt = $this::$pdo->prepare($this->sql['Restore_Password']);
		$stmt->bindValue(':email', $email, PDO::PARAM_STR);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch();
		return $row['password'];
	
	}
	
	function Show_art_and_article() {
		$stmt = $this::$pdo->prepare($this->sql['Show_art_and_article']);
		$stmt->execute();
		$row = $stmt->fetchAll();
		return $row;
	}
	
	function CheckWord($user, $id_word) {
		$stmt = $this::$pdo->prepare($this->sql['CheckWord']);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->bindValue(':id_word', $id_word);
		$stmt->execute();
		$row = $stmt->fetchAll();
		return $row;
	}
	
	function i_test($user, $c_ok, $c_not_ok, $licznik, $per_ok) {
		$stmt = $this::$pdo->prepare($this->sql['i_test']);
		$stmt->bindValue(':good', $c_ok, PDO::PARAM_INT);
		$stmt->bindValue(':bad', $c_not_ok, PDO::PARAM_INT);
		$stmt->bindValue(':licznik', $licznik, PDO::PARAM_INT);
		$stmt->bindValue(':percent', $per_ok, PDO::PARAM_INT);
		$stmt->bindValue(':user', $user, PDO::PARAM_STR);
		$stmt->bindValue(':date', date('Y-m-d'), PDO::PARAM_STR);
		$stmt->execute();
		return $row;
			
	}
	
	function show_results() {
		$stmt = $this::$pdo->prepare($this->sql['show_results']);
		$stmt->execute();
		$row = $stmt->fetchAll();
		return $row ;
	}
	
	
}
?>
