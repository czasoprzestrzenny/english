<meta charset="utf-8" />
<?php include('lib/layoutOpen.php');


 
class ContactForm {
	 
	protected $_errors = array();
	public $name;
	public $email;
	public $content;
	 
	public function execute() {
		$this->name = trim($_POST['name']);
		$this->email = trim($_POST['email']);
		$this->content = trim($_POST['content']);
		$this->validate();
		if(!$this->getErrors()){
			return $this->send();
		}
	}
	
	public function execute_password($pass_msg) {
		$this->name = trim($_POST['user']);
		$this->email = trim($_POST['email']);
		$this->content = trim($pass_msg);
		$this->validate();
		if(!$this->getErrors()){
			return $this->send_password();
		}
	}
	
	 
	public function validate() {
		if (empty($this->name))
			$this->setError('Pole imię i nazwisko nie może być puste.');
			if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
				$this->setError('Należy podać poprawny adres e-mail.');
				if (empty($this->content))
					$this->setError('Pole treść wiadomości nie może być puste.');
	}
	 
	public function send() {
		$headers = "Content-type: text/html; charset=utf-8";
		$title = "=?UTF-8?B?" . base64_encode("Kontakt w sprawie angielskiego") . "?=";
		$body = 'Wiadomość od: ' . $this->name . ' <br>';
		$body .= 'Adres e-mail: ' . $this->email . '<br>';
		$body .= 'Treść wiadomości: ' . $this->content . '<br>';
		$mail = mail('jarcyk@jarcyk.webd.pl', $title, $body, $headers);
		if($mail)
			return true;
			else {
				$this->setError('Przepraszamy, ale e-mail nie może zostać wysłany.');
				return false;
			}
	}
	 
	public function send_password() {
		$headers = "Content-type: text/html; charset=utf-8";
		$title = "=?UTF-8?B?" . base64_encode("Kontakt w sprawie angielskiego- przypomnienie hasła") . "?=";
		$body = 'Następujący użytkownik poprosił o przypomnienie hasła: ' . $this->name . ' <br>';
		$body .= 'Twój adres mail: ' . $this->email . '<br>';
		$body .= 'Twoje hasło do aplikacji: ' . $this->content . '<br>';
		$mail = mail($this->email, $title, $body, $headers);
		if($mail) {	echo '<div class="alert alert-success" role="alert"> sprawdz pocztę email- hasło zostało wysłane </div>';
			return true;}
			else {
				echo "nie znaleziono użytkownika, o podanym loginie i emailu";
				$this->setError('Przepraszamy, ale e-mail nie może zostać wysłany.');
				return false;
			}
	}
	
	public function setError($errorMsg) {
		$this->_errors[] = $errorMsg;
	}
	 
	public function getErrors() {
		return count($this->_errors) ? $this->_errors : false;
	}
	 
}

?>

