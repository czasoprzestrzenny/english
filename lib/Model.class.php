<?php

abstract class Model {
	static $dsn;
	static $user;
	static $password;
	
	protected static $pdo;
	
	protected $sql = array();
	
	function __construct() {
//		echo 'konstruktor Model';
		if (empty ( self::$pdo ) && ! empty ( self::$dsn )) {
			self::$pdo = new PDO ( self::$dsn, self::$user, self::$password, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
			) );
			self::$pdo->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		}
	}
}

?>
