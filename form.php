
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Formularz kontaktowy</title>
<link rel="Stylesheet" type="text/css" href="style/login.css" />
</head>

<body>
<?php
require_once 'lib/contactform.class.php';
$errors = false;
$success = false;
if(isset($_POST['sent'])){
	$contactForm = new ContactForm;
	$success = $contactForm->execute();
	$errors = $contactForm->getErrors();
}
if($errors)
	echo implode('<br>', $errors);
	if(!$success): ?>

    
    		<div class = "container">
	<div class="wrapper">
		<form action="" method="post" name="Login_Form" class="form-signin">       
		    <h3 class="form-signin-heading">Formularz kontaktowy</h3>
					  <hr class="colorgraph"><br>
		
		      <input type="text" class="form-control" name="name" placeholder="Imię i nazwisko" required="" autofocus="" />
			  <input type="email" name="email" id="email" class="form-control" placeholder="Twój adres email"/>
			   <input type="text" class="form-control" name="content" placeholder="Treść wiadomości" required="" autofocus="" />
			  <button class="btn btn-lg btn-primary btn-block"  name="sent"  type="submit">Wyślij wiadomość</button>  			
 		
	</div>
</div>


<?php else: ?>
echo '<div class="alert alert-success" role="alert"> Wiadomość została wysłana. </div>';

<?php endif; ?>