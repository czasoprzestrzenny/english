<div class="container">
<?php
session_start(); 
include_once("analyticstracking.php");
require_once 'lib/Model.class.php';
require_once 'lib/WordModel.class.php';
require_once 'config.php';
$user= $_SESSION['user_id'];
include('lib/layoutOpen.php');
$wm = new WordModel ();?>

<?php $t= $wm->countWords_user($user); 
if ($t==0) { ?> <meta http-equiv="refresh" content="0; index.php"> <?php ;} else {?>



<form class="form-horizontal" action="index.php" method="POST">
<fieldset>
<input type="hidden" name="licznik" value=-1>
<!-- Form Name -->
<legend>Opcja quiz</legend>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="radios">Czy chcesz uruchomić quiz</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="radios-0">
      <input type="radio" name="quiz" id="radios-0" value="1" checked="checked">
      Nie
    </label>
	</div>
  <div class="radio">
    <label for="radios-1">
      <input type="radio" name="quiz" id="radios-1" value="2">
      Tak
    </label>
	</div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Ile słówek ma składać się na quiz</label>  
  <div class="col-md-4">
  <input id="textinput" name="total"  placeholder="Wprowadź ilość pytań do quizu" class="form-control input-md">
 
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Zacznij</button>
  </div>
</div>

</fieldset>
</form>
</form>
</div>

<?php };?>





<div class="container">
  <h2>Wyniki quizów</h2>
  <p>Poniżej znajdują się wyniki najlepiej wykonanych quizów</p>            
  <table class="table">
    <thead>
      <tr>
		<th>Kiedy</th>
        <th>Użytkownik</th>
        <th>Ilość pytań</th>
        <th>Odpowiedzi dobre</th>
		<th>Odpowiedzi złe</th>
		<th>Wynik w procentach</th>
      </tr>
    </thead>
    <tbody>

<?php foreach ($wm->show_results() as $row): ?> 

      <tr>
        <td><?php echo $row['data'];?></td>
	    <td><?php echo $row['user'];?></td>
        <td><?php echo $row['licznik'];?></td>
        <td><?php echo $row['good'];?></td>
        <td><?php echo $row['bad'];?></td>
        <td><?php echo $row['percent'];?></td>
       </tr> 

<?php endforeach; ?> 

    </tbody>
  </table>
</div>







<?php include('lib/layoutClose.php');?>



